
let trainer = {
    name: 'Ask Ketchup',
    age: 10,
    friends: {
        hoenn: ['May', 'Max'],
        kanto: ['Brock', 'Misty']
    },
    pokemon: ['Pikachu', 'Squirtle', 'Bulbasour', 'Snorlax'],

    talk: function() {
        console.log(`${this.pokemon[0]}! I choose you!`)
    }
}
console.log(trainer);
console.log('Result of dot notation');
console.log(trainer.name);
console.log('Result of square bracket notation:');
console.log(trainer['pokemon']);
console.log('Result of talk method:');
trainer.talk();

function Pokemons(name, level, health, attack, tackle, faint) {
    this.name = name;
    this.level = level;
    this.health = 2 * health;
    this.attack = level;

    this.tackle = function(target) {
        console.log(`${this.name} tackled ${target.name}`);
        target.health -= this.attack;
        console.log(`${target.name}'s health is now reduced to ${target.health}`);

        if (target.health <= 0) {

            target.faint();
        }
     

    };
        this.faint = function() {
            console.log(`${this,name} fainted.`);
        }
    }
    


let pikachu = new Pokemons('Pikachu', 12, 24, 12);
console.log(pikachu);
let Squirtle = new Pokemons('Squirtle', 8, 16, 8);
console.log(Squirtle);
let Bulbasour = new Pokemons('Bulbasour', 100, 200, 100);
console.log(Bulbasour);


Squirtle.tackle(pikachu);
console.log(pikachu);
Bulbasour.tackle(Squirtle);
console.log(Squirtle);
Bulbasour.tackle(pikachu);





   

